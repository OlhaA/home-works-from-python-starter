"""Task3
Створіть програму-калькулятор, яка підтримує наступні операції:
+, -, *, /, зведення в ступінь, зведення до квадратного та кубічного коренів.
Всі дані повинні вводитися в циклі, доки користувач не вкаже, що хоче завершити виконання програми.
Кожна операція має бути реалізована у вигляді окремої функції.
Функція ділення повинна перевіряти дані на коректність та видавати повідомлення про помилку
у разі спроби поділу на нуль.
"""

bold, yellow, blue, red, reset = "\033[1m", "\033[33m", "\033[36m", "\033[31m", "\033[0m",


def num_sum(x, y):
    return x + y


def diff(x, y):
    return x - y


def prod(x, y):
    return x * y


def div(x, y):
    if not y:
        return "Cannot be divided by 0"
    else:
        return x / y


def pow(x, y):
    return x ** y


def sqrt(x):
    return -abs(x)**(1/2) if x < 0 else x ** (1/2)


def cbrt(x):
    return -abs(x)**(1/3) if x < 0 else x ** (1/3)


def is_float(val):
    try:
        float(val)
        return True
    except ValueError:
        return False


def is_valid_operator(op):
    # operators for 2 numbers
    return op in ['+', '-', '*', '/', '^']


def is_valid_op(op):
    # operators for 1 number
    return op in ['sqrt', 'cbrt']


def is_valid_expression(exp):
    if 3 < len(exp) < 2:
        return False

    if len(exp) == 2:
        return is_float(exp[0]) and is_valid_op(exp[1])

    if len(exp) == 3:
        return is_float(exp[0]) and is_float(exp[2]) and is_valid_operator(exp[1])


def get_expression():
    while True:
        expression = input(blue + "Enter num1 operator num2 by a space\n" +
                           yellow + "Available operators: "
                           "\n   +    |      -     |   *   |     /    |   ^   |    sqrt     |   cbrt "
                           "\n -------+------------+-------+----------+-------+-------------+--------------"
                           "\n-7 + 17 | 10.5 - 7.3 | 5 * 5 | 100 / 10 | 3 ^ 3 |   25 sqrt   |  -64 cbrt"
                           "\n                                        | Power | Square root |  Cube root" +
                          red + "\nEnter <of> to exit -> \033[0m" + reset).split()
        print()

        if expression == ["of"]:
            exit()

        if not is_valid_expression(expression):
            print("Incorrect Input. Try again")
            continue
        break

    if len(expression) == 2:
        expression[0] = float(expression[0])
        expression.append(0)
    if len(expression) == 3:
        expression[0] = float(expression[0])
        expression[2] = float(expression[2])

    return expression


def calculator(op, x, y):
    match op:
        case "+":
            print(bold + f"{x} + {y} = {num_sum(x, y)}" + reset)
        case "-":
            print(bold + f"{x} - {y} = {diff(x, y)}" + reset)
        case "*":
            print(bold + f"{x} * {y} = {prod(x, y):.2f}" + reset)
        case "/":
            print(bold + f"{x} / {y} = {round(div(x, y), 2) if is_float(div(x, y)) else div(x, y)}" + reset)
        case "^":
            print(bold + f"{x}^{y} = {pow(x, y):.2f}" + reset)
        case "sqrt":
            print(bold + f"Square root of {x} = {sqrt(x):.2f}" + reset)
        case "cbrt":
            print(bold + f"Cube root of {x} = {cbrt(x):.2f}" + reset)


while True:
    x, operator, y = get_expression()
    calculator(operator, x, y)
