"""Task3
Створіть дві функції, що обчислюють значення певних алгебраїчних виразів.
На екрані виведіть таблицю значень цих функцій від -5 до 5 з кроком 0.5.
"""


def cube(x):
    return x**3


def cube_root(x):
    return round(-abs(x)**(1/3), 2) if x < 0 else round(x**1/3, 2)


num = -5
while num < 5.5:
    print(f"{num:<4} in cube = {cube(num):<7} | Cube root of {num:<4} = {cube_root(num):<5}")
    num += 0.5
