"""Task1
Створіть функцію, яка відображає привітання для користувача із заданим ім'ям.
Якщо ім'я не вказано, вона повинна виводити привітання для користувача з Вашим ім'ям.
"""


def print_hello():
    name = input("Enter you name: ")
    print(f"Hello, {name}" if name else f"Hello, {'Olha'}")


print_hello()
