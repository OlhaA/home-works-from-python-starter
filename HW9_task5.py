"""Task5
Створіть функцію quadratic_equation, яка приймає на вхід 3 параметри: a, b, c.
Усередині цієї функції створити змінні x1, x2 зі значенням None
та функцію calc_rezult з формальними параметрами зовнішньої функції quadratic_equation.
Всередині функції calc_rezult здійснити пошук дискримінанта, згідно з результатом якого
зробити розрахунок коренів рівняння.
Зовнішня функція quadratic_equation має повернути перелік значень коренів квадратного рівняння.
Надати можливість користувачеві ввести з клавіатури формальні параметри для передачі їх
у створену функцію quadratic_equation, результати роботи функції відобразити на екрані.
"""


def quadratic_equation(a, b, c):
    """Return the root(s) of a quadratic equation"""
    x1, x2 = None, None

    def calc_result():
        """Calculate the roots of the quadratic equation"""
        nonlocal x1, x2
        d = b**2 - 4*a*c                            # d - discriminant

        if d > 0:
            x1 = round(-b + (d**0.5 / 2*a), 2)
            x2 = round(-b - (d**0.5 / 2*a), 2)
        elif d == 0:
            x1 = round(-b / 2*a, 2)

    calc_result()
    return x1, x2


def get_abc():
    """Return the values of a, b, c entered by the user."""
    while True:
        print("ax^2 + bx + c = 0")
        abc = input("Enter values of a, b, c by a space, e.g. 1 -7 6: ").split()

        if not is_valid_abc(abc):
            print("Incorrect input. Try again")
            continue
        break

    abc = tuple(map(int, abc))
    return abc


def print_roots(x1, x2):
    """Print the root(s) of a quadratic equation."""
    if x1 and x2:
        print(f"x1 = {x1}, x2 = {x2}")
    elif x1:
        print(f"x = {x1}")
    else:
        print("There aren't real roots")


def is_valid_abc(abc):
    """Check that the user has entered valid values of a, b, c"""
    def is_number(char):
        """Check is char a number"""
        try:
            float(char)
            return True
        except ValueError:
            return False

    if len(abc) != 3:
        return False
    return is_number(abc[0]) and is_number(abc[1]) and is_number(abc[2])


a, b, c = get_abc()
print_roots(*quadratic_equation(a, b, c))

# for test
# d > 0 -> abc = 1 -7 6 -> x1, x2
# d = 0 -> abc = 1 -4 4 -> x
# d < 0 -> abc = 5 5 5 -> There aren't real roots
