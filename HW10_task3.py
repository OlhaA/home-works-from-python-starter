""" Task3
Створіть інженерний калькулятор з використанням модуля math,
в якому передбачене меню. Під час створення дотримуйтесь
правил специфікації PEP 8.
"""

from math import sqrt, sin, cos, tan, log, exp, e, pi


YELLOW, BLUE, RED, = "\033[33m", "\033[36m", "\033[31m"
RESET, BOLD = "\033[0m", "\033[1m"

HELP = "\nAvailable operations:\n"\
       "                         | power |   root  |                          | natural |  e^x   |\n"\
       "|  +  |  -  |  *  |   /  |   **  | sqrt(x) | sin(x) | cos(x) | tan(x) | log(x)  | exp(x) |   ()    |\n"\
       "| 1+1 | 5-1 | 7*e | 8/pi |  3**2 | sqrt(4) | sin(3) | cos(5) | tan(7) | log(e)  | exp(3) | e*(9-3) |\n"

MENU = "Enter mathematical expression using available operations.\n"\
       "E.g. (5.2 + 3**2) / cos(6*pi/180) + sqrt(25)*log(5)\n"


def is_valid_expression(expression: str) -> bool:
    """Check that the user's expression is valid for that calculator"""

    def is_expression_executed():
        try:
            eval(expression)
            return True
        except (SyntaxError, NameError):
            print(RED
                  + "SyntaxError: Use only available operators and numbers."
                  + RESET)
            return False
        except ZeroDivisionError:
            print(RED
                  + "ZeroDivisionError: Division by 0."
                  + RESET)
            return False
        except Exception:
            print(RED +
                  "SyntaxError: Use only available operators and numbers."
                  + RESET)
            return False

    def is_operator(symbol):
        return symbol in "+-*/ sin cos tan sqrt log exp e pi ()."

    def split_up_operators_numbers() -> list:
        """Split up the expression into operators and numbers.
        E.g. 'sin(3.5)' -> ['sin', '(', '3', '.', '5', ')'] """
        operators_and_num = expression[0]

        for previous, next in zip(expression, expression[1:]):
            operators_and_num += (next if previous.isalpha() and next.isalpha()
                                  else ' ' + next)
        return operators_and_num.split()

    if not expression:
        print(RED + "Error: Empty expression" + RESET)
        return False

    for symbol in split_up_operators_numbers():
        if not symbol.isdigit() and not is_operator(symbol):
            print(RED
                  + "SyntaxError: Use only available operators and numbers."
                  + RESET)
            return False

    return is_expression_executed()


def calculate_expression(expression: str) -> float:
    """Return the result of evaluating the expression."""
    return eval(expression)


# Main program
def calculator():
    """Get mathematical expression from the user and print result."""

    while True:
        print(YELLOW + HELP + RESET)

        expression = input(BLUE
                           + MENU
                           + YELLOW
                           + "Enter <of> to exit: "
                           + RESET).strip()

        if expression.lower() == "of" or expression.lower() == "<of>":
            exit()

        if not is_valid_expression(expression):
            print(RED + "Try again." + RESET)
            continue

        print(BOLD + f"\nResult = {calculate_expression(expression)}" + RESET)


calculator()
