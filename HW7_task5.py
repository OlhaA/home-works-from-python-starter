"""Task5:
Є рядок, в якому зберігаються 1000 слів.
Створіть словник із ключами - унікальними словами
та значеннями - кількістю повторів кожного слова у послідовності.
"""

from collections import Counter

string = "Terry Berry Carry Merry merry " * 250

words_counter = dict(Counter(string.lower().split()))
print(words_counter)
