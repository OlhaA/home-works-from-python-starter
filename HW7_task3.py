"""Task3
Створіть програму, яка має 2 списки цілочисельних значень та друкує
список унікальних значень без повтору, які є в 1 списку (немає в другому) і навпаки.
"""


def get_unique(list_1, list_2):
    return list({*list_1}.difference(list_2)), list({*list_2}.difference(list_1))


numbers_1 = [1, 2, 3, 4, 5, 6, 7]
numbers_2 = [4, 5, 6, 7, 8, 9, 10]

print(f"First list: {numbers_1}, Second list: {numbers_2}\n")
print(f"Uniques values: {get_unique(numbers_1, numbers_2)}")
