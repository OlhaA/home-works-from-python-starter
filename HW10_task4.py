""" Task4
Створіть магазин канцтоварів використовуючи списки для зберігання елементів.
Для додавання елементів створіть функцію, яка буде запитувати дані в
користувача і зібрані дані у вигляді кортежу додавати у створений список на
початку. Результат вивести на екран. Під час створення дотримуйтесь правил
специфікації PEP 8.
"""

YELLOW, BLUE, RED, RESET = "\033[33m", "\033[36m", "\033[31m", "\033[0m"

stationery = [('paper', 'pen', 'pencil')]
available_items = ['color paper', 'glue', 'paper a4', 'pen',
                   'pencil', 'markers', 'folder', 'punched pocket']

help_info = f"\nAvailable items: {', '.join(available_items)}"


def add_stationery(items: tuple):
    """Add the tuple with stationery items to the stationery list."""
    stationery.insert(0, items)


def get_user_stationery() -> tuple:
    """Get the tuple with stationery items from user."""

    while True:
        print(YELLOW + help_info + RESET)
        items = input(BLUE
                      + "Enter the stationery items from Available items "
                        "by a comma and space, e.g: pen, paper A4, glue -> "
                      + RESET).strip().lower().split(", ")

        # If the user entered a <,> or <.> at the end -> del it
        items = tuple(del_coma_and_dot(items))

        if not is_available_items(items):
            print("Error: Choose products only from the list of "
                  "Available items, and enter them by a comma and a space")
            continue

        return items


def is_available_items(items: tuple) -> bool:
    """Check that the items are available,
    and we can add them to stationery list."""
    for item in items:
        if item not in available_items:
            return False
    return True


def del_coma_and_dot(items):
    """If the user entered a <,> or <.> at the end -> del it"""
    items[-1] = items[-1].replace(',', '').replace('.', '')
    return items


# Main program
def main_menu():
    while True:
        step = input(BLUE
                     + "\nEnter 1 to add the stationery items to list.\n"
                     + RED
                     + "Enter 2 to exit -> "
                     + RESET)

        match step:
            case "1":
                items = get_user_stationery()
                add_stationery(items)
                print(f"Stationery list = {stationery}")
            case "2":
                exit()
            case _:
                print("Choose step 1 or 2")


main_menu()
