"""Task2:
Створіть програму, яка емулює роботу сервісу зі скорочення посилань.
Повинна бути реалізована можливість введення початкового посилання та короткої назви
і отримання початкового посилання за її назвою.
"""


def is_it_new(link, name):
    """Check that the user link is new."""

    if link in links.values():
        print("This link already exists")

    if name in links:
        print("This name already used")

    return not(link in links.values() or name in links)


def add_link():
    """Add the link to link dictionary."""

    while True:
        link = input("Enter the link: ")
        link_name = input("Enter the link name: ")
        if is_it_new(link, link_name):
            links[link_name] = link
            print("The link was added")
            print(links)
            break
        print("Try again")


def print_link():
    name = input("Enter the link name: ")
    if name in links:
        print(f"Your link -> \033[34m{links[name]}\033[0m")
    else:
        print("There is no link with that name")


links = {}

# main menu
while True:
    step = input("\033[36mEnter: \n1 to create a new link, "
                       f"\n2 to get the link by name, "
                       f"\n3 to exit: \033[0m")
    print()

    match step:
        case "3":
            exit()

        case "1":
            add_link()

        case "2":
            print_link()

        case _:
            print("Choose step -> 1 or 2 or 3: ")
