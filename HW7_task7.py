"""Task7
Створіть прототип програми «Облік кадрів», в якій є можливість перегляду та внесення змін
до структури(реалізуйте інтерфейс(меню), за допомогою якого можна робити маніпуляції з даними):

прізвище:
посада: ...
досвід роботи: …
портфоліо: …
коефіцієнт ефективності: …
стек технологій: …
зарплата: …

Передбачте можливість виведення на екран сортування за прізвищем та найефективнішим співробітником.
"""

workers = [{"surname": "Crews", "position": "Senior developer", "experience": 5.0,
        "portfolio": "linkedin.com/in/terry-crews/", "efficiency": 1,
        "technology": "C++, Python", "salary": 5000.0},
           {"surname": "Black", "position": "Middle developer", "experience": 3.0,
        "portfolio": "linkedin.com/in/chelsea-black/", "efficiency": 0.9,
        "technology": "C++, Python", "salary": 3500.0}]


def print_workers(efficiency=0):
    # efficiency=0 -> Sort in alphabetical order, efficiency=1 -> Sort in order of efficiency
    if not efficiency:
        workers.sort(key=lambda worker: worker["surname"])
        for worker in workers:
            print(worker)
    else:
        workers.sort(key=lambda worker: worker["efficiency"], reverse=True)
        for worker in workers:
            print(worker)
    print()


def is_valid_info_point(inp):
    return inp in ["surname", "position", "experience", "efficiency", "technology", "salary"]


def is_valid_num(num):
    try:
        float(num)
        return True
    except ValueError:
        return False


def is_valid_word(word):
    for char in word.split():
        if not char.isalpha():
            return False
    return True


def worker_entering(inp_str, word=True):
    # word=True -> check as word, word=False check as number
    while True:
        inp = input(inp_str).strip()

        if word and is_valid_word(inp):
            return inp
        elif not word and is_valid_num(inp):
            return float(inp)

        print("Incorrect Input. Try again.")


def info_point_entering():
    while True:
        info_point = input("\nEnter info point which you want to change: "
                           "\nsurname, position, experience, efficiency, technology, salary)-> ").strip()
        if not is_valid_info_point(info_point):
            print("Incorrect Input. Try again")
            continue
        return info_point


def add_worker():
    worker = dict.fromkeys(("surname", "position", "experience", "portfolio",
                            "efficiency", "technology", "salary"))

    worker["surname"] = worker_entering("Enter the Surname of worker (alphabetic characters): ").title()
    worker["position"] = worker_entering("Enter the Position of worker (alphabetic characters): ").title()
    worker["experience"] = worker_entering("Enter work experience as years (number): ", False)
    worker["portfolio"] = input("Enter link to worker portfolio: ")
    worker["efficiency"] = worker_entering("Enter efficiency of worker (number): ", False)
    worker["technology"] = input("Enter technology stack: ")
    worker["salary"] = worker_entering("Enter worker salary (number): ", False)

    return worker


def edit_worker():
    surname = worker_entering("Enter the Surname of worker you want to change: ").title()
    point = info_point_entering()

    match point:
        case "surname":
            new_info = worker_entering("Enter the Surname of worker (alphabetic characters): ").title()
        case "position":
            new_info = worker_entering("Enter the Position of worker (alphabetic characters): ").title()
        case "technology":
            new_info = input("Enter technology stack: ")
        case "experience" | "efficiency" | "salary":
            new_info = worker_entering("Enter new info (number): ", False)

    for worker in workers:
        if worker["surname"] == surname:
            worker[point] = new_info
            print("Info was updated")
            return True
    print("\nThere is no employee with this surname\n")
    return False


def edit_info():
    while True:
        step = input("Enter:"
                     "\n1 to add new worker"
                     "\n2 to edit worker's info -> ")
        print()
        match step:
            case "1":
                workers.append(add_worker())
                print("\nWorker was added\n")
                break
            case "2":
                edit_worker()
                break
            case _:
                print("Choose step 1 or 2")


while True:
    step = input("Enter:"
                 "\n1 to see workers in alphabetical order"
                 "\n2 to see workers in order of their efficiency"
                 "\n3 to edit workers info"
                 "\n4 to exit -> ")
    print()
    match step:
        case "1":
            print_workers()
        case "2":
            print_workers(True)
        case "3":
            edit_info()
        case "4":
            exit()
        case _:
            print("Choose step 1, 2 or 3")
