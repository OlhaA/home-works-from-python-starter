"""Task5
Створіть програму, яка приймає як формальні параметри зріст і вагу користувача,
обчислює індекс маси тіла і в залежності від результату повертає інформаційне
повідомлення (маса тіла в нормі, недостатня вага або слідкуйте за фігурою).
Користувач з клавіатури вводить значення росту та маси тіла та передає ці дані
у вигляді фактичних параметрів під час виклику функції. Програма працює доти,
доки користувач не зупинить її комбінацією символів «off».
"""


def get_ibm(weight, height):
    return weight / height**2


def get_message(ibm):
    if ibm < 18.5:
        return "Insufficient weight"
    if ibm > 24.9:
        return "Keep track of your weight"
    else:
        return "Body weight is normal"


def is_float(val):
    try:
        float(val)
        return True
    except ValueError:
        return False


def is_valid_input(inp):
    if len(inp) != 2:
        return False
    return is_float(inp[0]) and is_float(inp[1])


while True:
    body = input("Enter your weight (kg), and height (m), by a space; e.g. 70 1.75: ").split()

    if not is_valid_input(body):
        print("Incorrect Input. Try again")
        continue

    body = tuple(map(float, body))
    print('\033[34m' + get_message(get_ibm(*body)) + '\033[0m')

    exit()
